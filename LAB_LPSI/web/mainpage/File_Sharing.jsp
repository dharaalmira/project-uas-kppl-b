<%-- 
    Document   : File_Sharing
    Created on : Dec 29, 2015, 5:21:14 PM
    Author     : Izzatun N
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Laboratorium Pemograman Sistem Informasi</title>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Abel|Satisfy' rel='stylesheet' type='text/css' />
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
    
    <body>
<div id="wrapper">
    <h1>Jurusan Sistem Informasi</h1>
       <p> Institut Teknologi Sepuluh Nopember </p>
       <div class="header-info">
            <ul class="support-right">
                <li><a href="index.jsp"> Welcome <%=session.getAttribute("username")%> <i class="glyphicon glyphicon-user" class="men">  </i>Logout</a></li>
            </ul>
       </div>
	<div id="header-wrapper">
		<div id="header" class="container">
			
			<div id="menu">
				<ul>
					<li ><a href="indexLOGIN.jsp">Home</a></li>
					<li ><a href="Event_Log.jsp">Event Log</a></li>
					<li ><a href="Schedule.jsp">Schedules</a></li> 
					<li class="current_page_item"><a href="File_Sharing.jsp">File Sharing</a></li>
                                        <li><a href="user.jsp">User</a></li>
                                        <li><a href="report.jsp">Report</a></li>
				</ul>
			</div>
		</div>
            <div id="banner">
            <div class="content"><img src="../images/Sharing_File.jpg" width="1000" height="400" alt="" /></div>
            </div>
	</div>
	<div id="page">
            <div class="clearfix"> </div>
					<!---->
				</div>
			<div class="clearfix"> </div>
		</div>	
 				<div class="clearfix"> </div>	
                        <!----> 
                        <div id="page">
                            <div class="clearfix"> </div>
                            <h2 class="title">FILE SHARING</h2>
                        </div>
                        <!---->
                        <div class="container">
                            <div class=" login-left">
                                <h3>Ketentuan Penggunaan File Sharing:</h3>
                               <TMPL_VAR site_name> Perjanjian Layanan ini menggambarkan syarat dan ketentuan yang kami tawarkan kepada Anda. Dengan menggunakan layanan kami, Pengguna setuju untuk terikat oleh syarat dan ketentuan berikut: <TMPL_VAR site_name>
                                        <ol> 
                                            <li>Kami berhak untuk menonaktifkan menghubungkan langsung pada account pengguna yang menggunakan bandwidth yang berlebihan atau sebaliknya menyalahgunakan sistem.</li>
                                            <li>Pornografi, gambar seksual dan jenis gambar atau video ofensif dilarang. Materi berhak cipta juga dilarang. Kami berhak untuk memutuskan konten yang tidak sesuai dan dapat menghapus gambar atau video setiap saat tanpa pemberitahuan Pengguna.</li>
                                            <li>Pengguna harus setuju untuk mematuhi semua undang-undang yang berlaku,termasuk hak cipta dan merek dagang hukum. Gambar, video dan file yang melanggar hak copy atau merek dagang tidak diperbolehkan. Jika seseorang memiliki klaim pelanggaran terhadap Anda, Anda akan diminta untuk menghapus file yang dilindungi hak cipta sampai masalah teratasi. Jika ada sengketa antara peserta di situs ini,kami tidak berkewajiban untuk terlibat.</li>
                                        </ol>
<!---->
<Script type="text/javascript">
var ext_allowed='<TMPL_VAR ext_allowed>';
var ext_not_allowed='<TMPL_VAR ext_not_allowed>';
var max_upload_files='<TMPL_VAR max_upload_files>';
var max_upload_size='<TMPL_VAR max_upload_size>';
var descr_mode='<TMPL_VAR enable_file_descr>';
</Script>

<h3>Form Upload</h3>
<p><b> Up to 1 file max </b></p>
<form enctype="multipart/form-data" action="uploadfile.jsp" method="post" onsubmit="return verify()">       
    <table border='1'>
        <tr>
            <td>
                <br/>
                File yang bisa di upload berupa file MS-word, MS-Excel, txt dan PDF files. max size = 15mb
                <br/>
                <br/>
            </td>
        </tr>
        <tr>
            <td>
                <input type="file" name="filename" id="filename" accept=".txt, application/pdf, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword"  />                                                            
            </td>
        </tr>
    </table>
    <p></p>
    <a>Saya telah membaca dan setuju dengan ketentuan diatas</a>
    <input type="checkbox" name="tos" value="1" id="tos" checked onClick="if(this.checked){$('submit_btn').disabled=false}else{$('submit_btn').disabled=true};this.blur();">
        <p><input type="submit" value="Save File"></p>
</form>
<br/>
<br/>
<h3>Download file</h3>
<a href="downloadfile.jsp"> Klik disini untuk melihat dan download file</a>
<div class="clearfix"> </div>

</script>
<a href="#to-top" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!----> 
<div style="clear: both;">&nbsp;</div>
    </body>
</html>
