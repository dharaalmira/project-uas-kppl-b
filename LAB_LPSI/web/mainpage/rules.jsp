<%-- 
    Document   : rules
    Created on : Dec 29, 2015, 5:18:23 PM
    Author     : Izzatun N
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Laboratorium Pemograman Sistem Informasi</title>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Abel|Satisfy' rel='stylesheet' type='text/css' />
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
    
    <body>
<div id="wrapper">
    <h1>Jurusan Sistem Informasi</h1>
       <p> Institut Teknologi Sepuluh Nopember </p>
	<div id="header-wrapper">
		<div id="header" class="container">
			<div id="logo">
			</div>
			<div id="menu">
				<ul>
					<li ><a href="index.jsp">Home</a></li>
					<li ><a href="halamanlogin.jsp">Login/Regist</a></li> 
					<li class="current_page_item"><a href="rules.jsp">LPSI Rules</a></li>
				</ul>
			</div>
		</div>
            <td><div class="clearfix"> </div>
                <font color =white face="Geneva, Arial" size="21">
                    <p>Peraturan Laboratorium Pemrograman Sistem Informasi</p>
                </font>
                <font color =white face="Geneva, Arial" size="2">
                            <p> 1. Semua yang berada di lab wajib menjaga kebersihan, kerapian dan keamanan lab.
                            <p> 2. Dilarang merubah, menambah setting user dan komputer baik lokal maupun jaringan.
                            <p> 3. Dilarang bermain game selama jam kuliah.
                            <p> 4. Dilarang nonton film selama jam kuliah.
                            <p> 5. Dilarang meninggalkan komputer dalam keadaan login.
                            <p> 6. Dilarang me-lock komputer tanpa seizin admin.
                            <p> 7. Dilarang membawa makanan dan minuman ke dalam Lab.
                            <p> 8. Dilarang tidur di dalam lab.
                            <p> 9. Dilarang mencabut kabel LAN, power dan semua yang digunakan pada komputer lab untuk kepentingan pribadi.
                            <p> 10. Dilarang bertindak senonoh atau bertentangan dengan norma-norma kesopanan dan kesusilaan yang berlaku di ITS.
                            </font>
            </td>
                    </div>
	</div>
	<div id="page">
		
		<div style="clear: both;">&nbsp;</div>
        </div>
	</div>
	<!-- end #page --> 
</body>
</html>
