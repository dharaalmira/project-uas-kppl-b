<%-- 
    Document   : Event_Log
    Created on : Dec 29, 2015, 3:58:52 PM
    Author     : Izzatun N
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Laboratorium Pemograman Sistem Informasi</title>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Abel|Satisfy' rel='stylesheet' type='text/css' />
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
    
    <body>
<div id="wrapper">
    <h1>Jurusan Sistem Informasi</h1>
       <p> Institut Teknologi Sepuluh Nopember </p>
       <div class="header-info">
            <ul class="support-right">
                <li><a href="index.jsp"> Welcome <%=session.getAttribute("username")%> <i class="glyphicon glyphicon-user" class="men">  </i>Logout</a></li>
            </ul>
        </div>
	<div id="header-wrapper">
		<div id="header" class="container">
			<div id="logo">
			</div>
			<div id="menu">
				<ul>
					<li ><a href="indexLOGIN.jsp">Home</a></li>
					<li class="current_page_item"><a href="Event_Log.jsp">Event Log</a></li>
					<li><a href="Schedule.jsp">Schedules</a></li>
                                        <li><a href="File_Sharing.jsp">File Sharing </a></li>
                                        <li><a href="user.jsp">User</a></li>
                                        <li><a href="report.jsp">Report</a></li>
				</ul>
			</div>
		</div>
            <div id="banner">
                <div class="content"><img src="../images/img02.jpg" width="1000" height="400" alt="" /></div>
            </div>
        </div>
            <div id="page">
                <div class="clearfix"> </div>
                <!---->
            </div>
            <div class="clearfix"> </div>
                
</div>
            <div id="page">
                <div class="clearfix"> </div>
                <h2 class="title">EVENT LOG LPSI</h2>
                <div style="clear: both;">&nbsp;</div>
                    
            </div>
	<!-- end #page --> 
        
<style type="text/css">
    body
    {
        font-family: arial;
    }

    th,td
    {
        margin: 0;
        text-align: center;
        border-collapse: collapse;
        outline: 1px solid #e3e3e3;
    }

    td
    {
        padding: 5px 10px;
    }

    th
    {
        background: #666;
        color: white;
        padding: 5px 10px;
    }

    td:hover
    {
        cursor: pointer;
        background: #666;
        color: white;
    }
    </style>
    
    <table width="80%" align="center" >
    <div id="head_nav">
    <tr>
        <th>Month (Year 2016)</th>
        <th>January</th>
        <th>February</th>
        <th>March</th>
        <th>April</th>
        <th>May</th>
        <th>June</th>
        <th>July</th>
        <th>August</th>
        <th>September</th>
        <th>October</th>
        <th>November</th>
        <th>December</th>   
    </tr>
</div>  

    <tr>
        <th>Week 1</td>
        <td> <a href="Schedule.jsp">Alpro Mon, 8 Jan</a></td>
          <td> <a href="Schedule.jsp">PSDP Tue, 3 Feb</a></td>
            <td title="No Class" class="Holiday"></td>
         <td> <a href="Schedule.jsp">PBO Thu, 2 Apr</a></td>
          <td> <a href="Schedule.jsp">KPPL Fri, 3 May</a></td>
            <td>Training ISO 27001 3 June</td>
            <td>Training ISO 27001 4 July</td>
            <td>Training ISO 27001 3 August</td>
           <td title="No Class" class="Holiday"></td>
           <td title="No Class" class="Holiday"></td>
           <td>Training ISO 27001 3 November</td>
           <td title="No Class" class="Holiday"></td>
        </div>
    </tr>

    <tr>
        <th>Week 2</td>
        <td> <a href="Schedule.jsp">Alpro Mon, 15 Jan</a></td>
          <td> <a href="Schedule.jsp">DMJK and PSDP Tue, 10 Feb</a></td>
            <td title="No Class" class="Holiday"></td>
            <td> <a href="Schedule.jsp">PBO Thu, 9 Apr</a></td>
          <td> <a href="Schedule.jsp">KPPL Fri, 10 May</a></td>
            <td>Training ISO 27001 10 June</td>
            <td>Training ISO 27001 11 July</td>
            <td>Training ISO 27001 14 August</td>
           <td title="No Class" class="Holiday"></td>
           <td title="No Class" class="Holiday"></td>
           <td>Training ISO 27001 10 November</td>
           <td title="No Class" class="Holiday"></td>

        </div>
    </tr>

    <tr>
        <th>Week 3</td>
        
            <td> <a href="Schedule.jsp">Alpro Mon, 22 Jan</a></td>
          <td> <a href="Schedule.jsp">DMJK and PSDP Tue, 17 Feb</a></td>
            <td title="No Class" class="Holiday"></td>
            <td> <a href="Schedule.jsp">PBO Thu, 16 Apr</a></td>
          <td> <a href="Schedule.jsp">KPPL Fri, 17 May</a></td>
            <td>Training ISO 27001 17 June</td>
            <td>Training ISO 27001 18 July</td>
            <td>Training ISO 27001 21 August</td>
           <td title="No Class" class="Holiday"></td>
           <td title="No Class" class="Holiday"></td>
           <td>Training ISO 27001 17 November</td>
           <td title="No Class" class="Holiday"></td>
        </div>
    </tr>

    <tr>
        <th>Week 4</td>
        
           <td> <a href="Schedule.jsp">Alpro Mon, 29 Jan</a></td>
          <td> <a href="Schedule.jsp">PSDP Tue, 24 Feb</a></td>
            <td title="No Class" class="Holiday"></td>
            <td> <a href="Schedule.jsp">PBO Thurs, 23 Apr</a></td>
          <td> <a href="Schedule.jsp">KPPL Fri, 24 May</a></td>
            <td>Training ISO 27001 24 June</td>
              <td title="No Class" class="Holiday"></td>
            <td>Training ISO 27001 28 August</td>
           <td title="No Class" class="Holiday"></td>
           <td title="No Class" class="Holiday"></td>
           <td>Training ISO 27001 24 November</td>
           <td title="No Class" class="Holiday"></td>
           
        </div>
    </tr>
</table>
            <div>
		<a href="editEvent.jsp">Edit</a>
                <a href="deleteEvent.jsp">Delete</a>
			</div>
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page --> 
</body>
</html>
