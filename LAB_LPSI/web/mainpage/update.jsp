<%-- 
    Document   : Event_Log
    Created on : Dec 29, 2015, 3:58:52 PM
    Author     : Izzatun N
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Laboratorium Pemograman Sistem Informasi</title>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Abel|Satisfy' rel='stylesheet' type='text/css' />
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
    
    <body>
<div id="wrapper">
    <h1>Jurusan Sistem Informasi</h1>
       <p> Institut Teknologi Sepuluh Nopember </p>
       <div class="header-info">
            <ul class="support-right">
                <li><a href="index.jsp"> Welcome <%=session.getAttribute("username")%> <i class="glyphicon glyphicon-user" class="men">  </i>Logout</a></li>
            </ul>
        </div>
	<div id="header-wrapper">
		<div id="header" class="container">
			<div id="logo">
			</div>
			<div id="menu">
				<ul>
					<li ><a href="indexLOGIN.jsp">Home</a></li>
					<li ><a href="Event_Log.jsp">Event Log</a></li>
					<li><a href="Schedule.jsp">Schedules</a></li>
                                        <li><a href="File_Sharing.jsp">File Sharing </a></li>
                                        <li class="current_page_item"><a href="user.jsp">User</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div id="page">
		
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page --> 
</div>
</body>
</html>