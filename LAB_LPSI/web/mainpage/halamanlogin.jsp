<%-- 
    Document   : halamanlogin
    Created on : Dec 28, 2015, 10:40:30 AM
    Author     : Izzatun N
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Laboratorium Pemograman Sistem Informasi</title>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Abel|Satisfy' rel='stylesheet' type='text/css' />
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
    
    <body>
<div id="wrapper">
    <h1>Jurusan Sistem Informasi</h1>
       <p> Institut Teknologi Sepuluh Nopember </p>
	<div id="header-wrapper">
		<div id="header" class="container">
			<div id="logo">
			</div>
			<div id="menu">
				<ul>
                                    <li ><a href="index.jsp">Home</a></li>
                                    <li class="current_page_item"><a href="halamanlogin.jsp">Login/Regist</a></li>
                                    <li><a href="rules.jsp">LPSI Rules</a></li>
				</ul>
			</div>
		</div>
        <!-- tabel regist -->     
        <body>
            <form method="post" action="logn.jsp">
                <center>
                    <table border="1" width="50%" cellpadding="3">
                        <thead>
                            <tr>
                                <th colspan="2"><font color =white face="Geneva, Arial" size="4">
                                        Silahkan Login Disini!</font></th>

                            </tr>
                        </thead>
                        <tbody>
                    <tr>
                        <td><font color =white>User Name </font>
                        </td>
                        <td><input type="text" name="uname" value="" /></td>
                    </tr>
                    <tr>
                        <td><font color =white>Password </font>
                        </td>
                        <td><input type="password" name="pass" value="" /></td>
                    </tr>
                    <tr>
                        <td><input type="reset" value="Reset" /></td>
                        <td><input type="submit" value="Login" /></td>
                    </tr>
                    <tr>
                        <td colspan="2"><font color =white face="Geneva, Arial">
                                        Belum Register? </font> <a href="reg.jsp">Register Disini!</a></td>
                    </tr>
                </tbody>
            </table>
            </center>
        </form>
		<div style="clear: both;">&nbsp;</div>
	</div>
    </body>
	</div>
	<div id="page">
		
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page --> 
</body>
</html>