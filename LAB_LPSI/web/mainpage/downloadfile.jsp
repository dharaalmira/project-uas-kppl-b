<%-- 
    Document   : downloadfile
    Created on : Dec 30, 2015, 10:50:35 PM
    Author     : Izzatun N
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="conPackage.MyConnection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Laboratorium Pemograman Sistem Informasi</title>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Abel|Satisfy' rel='stylesheet' type='text/css' />
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
    
    <body>
<div id="wrapper">
    <h1>Jurusan Sistem Informasi</h1>
       <p> Institut Teknologi Sepuluh Nopember </p>
       <div class="header-info">
            <ul class="support-right">
                <li><a href="index.jsp"> Welcome <%=session.getAttribute("username")%> <i class="glyphicon glyphicon-user" class="men">  </i>Logout</a></li>
            </ul>
       </div>
	<div id="header-wrapper">
		<div id="header" class="container">
			
			<div id="menu">
				<ul>
					<li ><a href="index.jsp">Home</a></li>
					<li ><a href="Event_Log.jsp">Event Log</a></li>
					<li ><a href="Schedule.jsp">Schedules</a></li> 
					<li class="current_page_item"><a href="File_Sharing.jsp">File Sharing</a></li>
				</ul>
			</div>
		</div>
            <div id="banner">
            <div class="content"><img src="../images/Sharing_File.jpg" width="1000" height="400" alt="" /></div>
            </div>
	</div>
<div class="clearfix"> </div>
					<!---->
				</div>
			<div class="clearfix"> </div>
		</div>
		</div>	
 				<div class="clearfix"> </div>	
			</div>
<!---->
<div class="back">
	<h2>FILE SHARING - VIEW FILE</h2>
</div>
    <center>
        <p>You can download this</p>
        <table border="1">
            <tr><th>File Name</th><th>Uploaded By</th><th>File Type</th><th>Upload Time</th><th>Action</th></tr>
            <%
                ResultSet rs = MyConnection.getResultFromSqlQuery("select doc_id,filename,type,upload_time,upload_by from sharing");
                int count =0;
                while(rs.next())
                {
                   out.println("<tr>"
                           + "<td>"+rs.getString(2)+"</td>"
                           + "<td>"+rs.getString(5)+"</td>"
                           + "<td>"+rs.getString(3)+"</td>"
                           + "<td>"+rs.getString(4)+"</td>"
                           + "<td><a href='download.jsp?Doc_id="+rs.getInt(1) +"'> Download </a></td>"                                                      
                           + "</tr>");
                   count++;
                }
                rs.close();
                MyConnection.CloseConnection();
                if(count==0)
                {
                    out.println("<tr><td colspan='4'> No File Found..!! </td></tr>");
                }
            %>            
        </table>
        <br/>
        <a href="File_Sharing.jsp">Back to Upload </a>
    </center>
        <script type="text/javascript">
		$(document).ready(function() {
				/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
				*/
		$().UItoTop({ easingType: 'easeOutQuart' });
});
</script>
<a href="#to-top" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!----> 
<div style="clear: both;">&nbsp;</div>
    </body>
</html>

