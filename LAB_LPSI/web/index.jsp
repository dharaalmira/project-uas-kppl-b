<%-- 
    Document   : index
    Created on : Dec 27, 2015, 1:28:20 PM
    Author     : User
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by TEMPLATED
http://templated.co
Released for free under the Creative Commons Attribution License

Name       : Consecutive 
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20121231

-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Laboratorium Pemograman Sistem Informasi</title>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Abel|Satisfy' rel='stylesheet' type='text/css' />
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="wrapper">
	<div id="header-wrapper">
		<div id="header" class="container">
			<div id="logo">
				<h1>Selamat Datang di Laboratorium Pemograman Sistem Informasi</h1>
                                <p> Sistem Informasi - Institut Teknologi Sepuluh Nopember </p>
			</div>
			<div id="menu">
				<ul>
					<li class="current_page_item"><a href="#">Homepage</a></li>
					<li><a href="#">Services</a></li>
					<li><a href="#">Portfolio</a></li>
					<li><a href="#">About</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
			</div>
		</div>
		<div id="banner">
			<div class="content"><img src="images/img02.jpg" width="1000" height="400" alt="" /></div>
		</div>
	</div>
	<!-- end #header -->
        <div id="page">
		<div id="content">
			<div class="post">
                            <h2 class="title"><a href="#">Silahkan Login Disini! </a></h2>
	<form method="post" action="logn.jsp">
            <center>
            <table border="1" width="50%" cellpadding="3">
                <thead>
                    <tr>
                        <th colspan="2"><font color =black face="Geneva, Arial" size="4">
                            Silahkan Login Disini!</font></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>User Name</td>
                        <td><input type="text" name="uname" value="" /></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><input type="password" name="pass" value="" /></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Login" /></td>
                        <td><input type="reset" value="Reset" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">Belum Register? <a href="reg.jsp">Register Disini!</a></td>
                    </tr>
                </tbody>
            </table>
            </center>
        </form>
                            	</div>
			</div>
		</div>

</body>
</html>

   
       