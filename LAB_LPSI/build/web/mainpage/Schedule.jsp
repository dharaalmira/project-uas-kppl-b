<%-- 
    Document   : Schedule
    Created on : Dec 29, 2015, 5:20:49 PM
    Author     : Izzatun N
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Laboratorium Pemograman Sistem Informasi</title>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Abel|Satisfy' rel='stylesheet' type='text/css' />
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
    
    <body>
<div id="wrapper">
    <h1>Jurusan Sistem Informasi</h1>
       <p> Institut Teknologi Sepuluh Nopember </p>
       <div class="header-info">
            <ul class="support-right">
                <li><a href="index.jsp"> Welcome <%=session.getAttribute("username")%> <i class="glyphicon glyphicon-user" class="men">  </i>Logout</a></li>
            </ul>
       </div>
	<div id="header-wrapper">
		<div id="header" class="container">
			<div id="logo">
			</div>
			<div id="menu">
				<ul>
					<li ><a href="indexLOGIN.jsp">Home</a></li>
					<li ><a href="Event_Log.jsp">Event Log</a></li>
					<li class="current_page_item"><a href="Schedule.jsp">Schedules</a></li> 
					<li><a href="File_Sharing.jsp">File Sharing</a></li>
				</ul>
			</div>
		</div>
	
               
		
            
	
		
		<div style="clear: both;">&nbsp;</div>
	
    <style type="text/css">
    body
    {
        font-family: arial;
    }

    th,td
    {
        margin: 0;
        text-align: center;
        border-collapse: collapse;
        outline: 1px solid #e3e3e3;
    }

    td
    {
        padding: 5px 10px;
        
    }

    th
    {
        background: #666;
        color: white;
        padding: 5px 10px;
    }

    td:hover
    {
        cursor: pointer;
        background: #666;
        color: white;
    }
    </style>
    <body>
        <center>
                    
                        <thead>
                            <tr>
                                <th colspan="2"><font color =white face="Geneva, Arial" size="4">
                                        Schedule LPSI</font></th>
                            </tr>
                        </thead>
    <table width="80%" align="center" >
    <div id="head_nav">
    <tr>
        <th>Time</th>
        <th> 08am - 11am</th>
        <th> 11pm - 13pm</th>
        <th> 13pm - 15pm</th>
        <th> 15pm - 17pm</th>
        <th> 17pm - 19pm</th>
        <th> 19pm - 21pm</th>
        
    </tr>
    </div>  

    <tr>
        <th>Monday</th>
        
            <td>Alpro</td>
            <td>PSDP</td>
            <td title="No Class" class="Holiday"></td>
            <td>PBO</td>
            <td>KPPL</td>
            <td title="No Class" class="Holiday"></td>     
        
    </tr>

    <tr>
        <th>Tuesday</th>
       
            <td>Alpro</td>
            <td>PSDP</td>
            <td title="No Class" class="Holiday"></td>
            <td>PBO</td>
            <td>KPPL</td>
            <td title="No Class" class="Holiday"></td>
        
    </tr>

    <tr>
        <th>Wednesday</th>
        
           <td>Alpro</td>
            <td>PSDP</td>
            <td title="No Class" class="Holiday"></td>
            <td>PBO</td>
            <td>KPPL</td>
            <td title="No Class" class="Holiday"></td>

       
    </tr>

    <tr>
        <th>Thursday</th>
        
            <td>Alpro</td>
            <td>PSDP</td>
            <td title="No Class" class="Holiday"></td>
            <td>PBO</td>
            <td>KPPL</td>
            <td title="No Class" class="Holiday"></td>

        
    </tr>
        </table>
        

            <div>
                <p><input type="button" name="edit" value="Edit" onClick="javascript:window.location='editSchedule.jsp';" style="background-color:#49743D;font-weight:bold;color:blue;">
              <!--  <p><input type="submit" value="Edit Schedule" ><a href="editSchedule.jsp"></a> </p>    -->           
            </div> 
              
            
<div style="clear: both;">&nbsp;</div>
            </body>
        </div>
        
            <div id="page">
            <div style="clear: both;">&nbsp;</div>
	</div>
           
</body>
      
</html>

