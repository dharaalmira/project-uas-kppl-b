<%-- 
    Document   : Event_Log
    Created on : Dec 29, 2015, 3:58:52 PM
    Author     : Izzatun N
--%>

<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Laboratorium Pemograman Sistem Informasi</title>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Abel|Satisfy' rel='stylesheet' type='text/css' />
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
    
    <body>
<div id="wrapper">
    <h1>Jurusan Sistem Informasi</h1>
       <p> Institut Teknologi Sepuluh Nopember </p>
       <div class="header-info">
            <ul class="support-right">
                <li><a href="index.jsp"> Welcome <%=session.getAttribute("username")%> <i class="glyphicon glyphicon-user" class="men">  </i>Logout</a></li>
            </ul>
        </div>
	<div id="header-wrapper">
		<div id="header" class="container">
			<div id="logo">
			</div>
			<div id="menu">
				<ul>
					<li ><a href="indexLOGIN.jsp">Home</a></li>
					<li ><a href="Event_Log.jsp">Event Log</a></li>
					<li><a href="Schedule.jsp">Schedules</a></li>
                                        <li><a href="File_Sharing.jsp">File Sharing </a></li>
                                        <li class="current_page_item"><a href="user.jsp">User</a></li>
                                        <li><a href="report.jsp">Report</a></li>
				</ul>
			</div>
		</div>
            <div id="banner">
                <div class="content"><img src="../images/img02.jpg" width="1000" height="400" alt="" /></div>
            </div>
	</div>
<div class="clearfix"> </div>
					<!---->
				</div>
			<div class="clearfix"> </div>
<!---->
<div class="clearfix"> </div>
					<!---->
				</div>
			<div class="clearfix"> </div>
		</div>
		</div>	
 				<div class="clearfix"> </div>	
			</div>
<!---->
<div class="back">
	<h2>USER MANAGEMENT</h2>
</div>
		<!---->
		<div class="container">
            <%
            try {
                String Host = "jdbc:mysql://localhost:3306/members";
                Connection connection = null;
                Statement statement = null;
                ResultSet rs = null;
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection(Host, "root", "");
                statement = connection.createStatement();
                String Data = "select * from members";
                rs = statement.executeQuery(Data);
            %>
            
            <table border="1" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <th>NRP</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Username</th>
                    <th>Password</th>
                    
                    <th>Aksi</th>
                </tr>
                <%
                while (rs.next()) {
                %>
                <tr>
                    <td><%=rs.getString("NRP")%></td>
                    <td><%=rs.getString("Nama")%></td>
                    <td><%=rs.getString("Email")%></td>
                    <td><%=rs.getString("Username")%></td>
                    <td><%=rs.getString("Password")%></td>
                    
                </tr>
                <%   }    %>
            </table>
            <%
                rs.close();
                statement.close();
                connection.close();
            } catch (Exception ex) {
                out.println("Can't connect to database.");
            }
            %>
        </div>
        
<script type="text/javascript">
		$(document).ready(function() {
				/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
				*/
		$().UItoTop({ easingType: 'easeOutQuart' });
});
</script>
<a href="#to-top" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!----> 
    </body>
</html>
        
        