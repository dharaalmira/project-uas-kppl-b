<%-- 
    Document   : success
    Created on : Dec 27, 2015, 8:48:07 PM
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<%
    if ((session.getAttribute("username") == null) || (session.getAttribute("username") == "")) {
%>
You are not logged in<br/>
<a href="Utama_Login.jsp">Please Login</a>
<%} else {
%>

<%-- 
    Document   : index
    Created on : Dec 27, 2015, 1:28:20 PM
    Author     : User
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by TEMPLATED
http://templated.co
Released for free under the Creative Commons Attribution License

Name       : Consecutive 
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20121231

-->
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Laboratorium Pemograman Sistem Informasi</title>
            <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css" />
            <link href='http://fonts.googleapis.com/css?family=Abel|Satisfy' rel='stylesheet' type='text/css' />
            <link href="style.css" rel="stylesheet" type="text/css" media="screen" />
    </head>
    <body
        <div id="wrapper">
            <h1>Jurusan Sistem Informasi</h1>
            <p> Institut Teknologi Sepuluh Nopember </p>
        </div>
        <div class="header-info">
            <ul class="support-right">
                <li><a href="index.jsp"> Welcome <%=session.getAttribute("username")%> <i class="glyphicon glyphicon-user" class="men">  </i>Logout</a></li>
            </ul>
        </div>
            <div id="header-wrapper">
                <div id="header" class="container">
                    <div id="logo">
                    </div>
                    <div id="menu">
                        <ul>
                            <li class="current_page_item"><a href="indexLOGIN.jsp">Home</a></li>
                            <li><a href="Event_Log.jsp">Event Log</a></li>
                            <li><a href="Schedule.jsp">Schedule</a></li> 
                            <li><a href="File_Sharing.jsp">File Sharing</a></li>
                            <li><a href="user.jsp">User</a></li>
                            <li><a href="report.jsp">Report</a></li>
                        </ul>
                    </div>
                </div>
                <div id="banner">
                    <div class="content"><img src="../images/img02.jpg" width="1000" height="400" alt="" /></div>
                </div>
        </div>
            <div id="page">
            <div id="content">
                <div class="post">
                    <h2 class="title" ><a href="#"> Selamat Datang di Laboratorium Pemograman Sistem Informasi </a></h2>
                    <div class="entry">
                        <p>Sampai dengan 2014, Jurusan Sistem Informasi ITS Surabaya memiliki fasilitas 4 laboratorium dan salah satunya yaitu Laboratorium Pemrograman Sistem Informasi atau biasa disebut dengan LPSI. LPSI merupakan laboratorium yang digunakan sebagai sarana penunjang untuk melakukan aktivitas pembelajaran dalam kuliah di Jurusan Sistem Informasi ITS Surabaya</p>
			<p>Selain digunakan sebagai sarana penunjang pembelajaran mahasiswa, lab ini juga sering digunakan untuk mengerjakan beberapa tugas di luar praktikum yang telah terjadwal.<br />
                        </p>
                    </div>
                    
                    <h2 class="title"><a href="#"> Contact </a></h2>
                    <div class="entry">
                        <p><img src="../images/telepon.jpg" width="25" height="25"> 081-232-1311-32</img>
                        </p>
                    </div>
                </div>
            </div>
            <div style="clear: both;">&nbsp;</div>
                    
        </div>
</body>
</html>
                <a href="logout.jsp">Log out</a>
<%
    }
%>
</html>
