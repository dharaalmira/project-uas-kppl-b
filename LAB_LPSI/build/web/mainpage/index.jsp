<%-- 
    Document   : index
    Created on : Dec 27, 2015, 1:28:20 PM
    Author     : User
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Laboratorium Pemograman Sistem Informasi</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Abel|Satisfy' rel='stylesheet' type='text/css' />
    <link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body
    <div id="wrapper">
        <h1>Jurusan Sistem Informasi</h1>
        <p> Institut Teknologi Sepuluh Nopember </p>
        <div id="header-wrapper">
            <div id="header" class="container">
                <div id="logo">
                </div>
                <div id="menu">
                    <ul>
                        <li class="current_page_item"><a href="#">Home</a></li>
                        <li><a href="halamanlogin.jsp">Login/Regist</a></li>
			<li><a href="rules.jsp">LPSI Rules</a></li>
                    </ul>
                </div>
            </div>
            <div id="banner">
                <div class="content"><img src="../images/img02.jpg" width="1000" height="400" alt="" /></div>
            </div>
        </div>
        <div id="page">
            <div class="clearfix"> </div>
            <!---->
        </div>
        <div class="clearfix"> </div>
            
    </div>
    <!---->
    <div id="page">
        <div class="clearfix"> </div>
        <h2 class="title">Selamat Datang di Laboratorium Pemrograman Sistem Informasi</h2>
        <div class="container">
            <div class=" login-left">
                <div class="back">
                    <div class="login-left">
                        <div class="entry">
                            <p>Sampai dengan tahun 2014, Jurusan Sistem Informasi ITS Surabaya memiliki fasilitas 4 laboratorium dan salah satunya yaitu Laboratorium Pemrograman Sistem Informasi atau biasa disebut dengan LPSI. LPSI merupakan laboratorium yang digunakan sebagai sarana penunjang untuk melakukan aktivitas pembelajaran dalam kuliah di Jurusan Sistem Informasi ITS Surabaya</p>
                            <p>Selain digunakan sebagai sarana penunjang pembelajaran mahasiswa, lab ini juga sering digunakan untuk mengerjakan beberapa tugas di luar praktikum yang telah terjadwal.<br />
                            </p>
                        </div>
                    </div>
                </div>
    <!---->
    <div id="page">
        <div class="clearfix"> </div>
        <h2 class="title">Contact</h2>
        <div class="entry">
            <p><img src="../images/telepon.jpg" width="25" height="25"> 081-232-1311-32</img>
            </p>
        </div>
    </div>
                </div>
                <div style="clear: both;">&nbsp;</div>
                    
            </div>
                
        </div>
            
    </div>
</body>
</html>

   
       