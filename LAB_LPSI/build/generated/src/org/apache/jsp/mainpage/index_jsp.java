package org.apache.jsp.mainpage;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n");
      out.write("<head>\r\n");
      out.write("    <meta name=\"keywords\" content=\"\" />\r\n");
      out.write("    <meta name=\"description\" content=\"\" />\r\n");
      out.write("    <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\r\n");
      out.write("    <title>Laboratorium Pemograman Sistem Informasi</title>\r\n");
      out.write("    <link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,300,600\" rel=\"stylesheet\" type=\"text/css\" />\r\n");
      out.write("    <link href='http://fonts.googleapis.com/css?family=Abel|Satisfy' rel='stylesheet' type='text/css' />\r\n");
      out.write("    <link href=\"style.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\r\n");
      out.write("</head>\r\n");
      out.write("<body\r\n");
      out.write("    <div id=\"wrapper\">\r\n");
      out.write("        <h1>Jurusan Sistem Informasi</h1>\r\n");
      out.write("        <p> Institut Teknologi Sepuluh Nopember </p>\r\n");
      out.write("        <div id=\"header-wrapper\">\r\n");
      out.write("            <div id=\"header\" class=\"container\">\r\n");
      out.write("                <div id=\"logo\">\r\n");
      out.write("                </div>\r\n");
      out.write("                <div id=\"menu\">\r\n");
      out.write("                    <ul>\r\n");
      out.write("                        <li class=\"current_page_item\"><a href=\"#\">Home</a></li>\r\n");
      out.write("                        <li><a href=\"halamanlogin.jsp\">Login/Regist</a></li>\r\n");
      out.write("\t\t\t<li><a href=\"rules.jsp\">LPSI Rules</a></li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("            <div id=\"banner\">\r\n");
      out.write("                <div class=\"content\"><img src=\"../images/img02.jpg\" width=\"1000\" height=\"400\" alt=\"\" /></div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("        <div id=\"page\">\r\n");
      out.write("            <div class=\"clearfix\"> </div>\r\n");
      out.write("            <!---->\r\n");
      out.write("        </div>\r\n");
      out.write("        <div class=\"clearfix\"> </div>\r\n");
      out.write("            \r\n");
      out.write("    </div>\r\n");
      out.write("    <!---->\r\n");
      out.write("    <div id=\"page\">\r\n");
      out.write("        <div class=\"clearfix\"> </div>\r\n");
      out.write("        <h2 class=\"title\">Selamat Datang di Laboratorium Pemrograman Sistem Informasi</h2>\r\n");
      out.write("        <div class=\"container\">\r\n");
      out.write("            <div class=\" login-left\">\r\n");
      out.write("                <div class=\"back\">\r\n");
      out.write("                    <div class=\"login-left\">\r\n");
      out.write("                        <div class=\"entry\">\r\n");
      out.write("                            <p>Sampai dengan tahun 2014, Jurusan Sistem Informasi ITS Surabaya memiliki fasilitas 4 laboratorium dan salah satunya yaitu Laboratorium Pemrograman Sistem Informasi atau biasa disebut dengan LPSI. LPSI merupakan laboratorium yang digunakan sebagai sarana penunjang untuk melakukan aktivitas pembelajaran dalam kuliah di Jurusan Sistem Informasi ITS Surabaya</p>\r\n");
      out.write("                            <p>Selain digunakan sebagai sarana penunjang pembelajaran mahasiswa, lab ini juga sering digunakan untuk mengerjakan beberapa tugas di luar praktikum yang telah terjadwal.<br />\r\n");
      out.write("                            </p>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("    <!---->\r\n");
      out.write("    <div id=\"page\">\r\n");
      out.write("        <div class=\"clearfix\"> </div>\r\n");
      out.write("        <h2 class=\"title\">Contact</h2>\r\n");
      out.write("        <div class=\"entry\">\r\n");
      out.write("            <p><img src=\"../images/telepon.jpg\" width=\"25\" height=\"25\"> 081-232-1311-32</img>\r\n");
      out.write("            </p>\r\n");
      out.write("        </div>\r\n");
      out.write("    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div style=\"clear: both;\">&nbsp;</div>\r\n");
      out.write("                    \r\n");
      out.write("            </div>\r\n");
      out.write("                \r\n");
      out.write("        </div>\r\n");
      out.write("            \r\n");
      out.write("    </div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
      out.write("\r\n");
      out.write("   \r\n");
      out.write("       ");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
