package org.apache.jsp.mainpage;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class indexLOGIN_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
      out.write("<head>\n");
      out.write("    <meta name=\"keywords\" content=\"\" />\n");
      out.write("    <meta name=\"description\" content=\"\" />\n");
      out.write("    <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\n");
      out.write("    <title>Laboratorium Pemograman Sistem Informasi</title>\n");
      out.write("    <link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,300,600\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("    <link href='http://fonts.googleapis.com/css?family=Abel|Satisfy' rel='stylesheet' type='text/css' />\n");
      out.write("    <link href=\"style.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n");
      out.write("</head>\n");
      out.write("<body\n");
      out.write("    <div id=\"wrapper\">\n");
      out.write("        <h1>Jurusan Sistem Informasi</h1>\n");
      out.write("        <p> Institut Teknologi Sepuluh Nopember </p>\n");
      out.write("        <div class=\"header-info\">\n");
      out.write("            <ul class=\"support-right\">\n");
      out.write("                <li><a href=\"index.jsp\"> Welcome ");
      out.print(session.getAttribute("username"));
      out.write(" <i class=\"glyphicon glyphicon-user\" class=\"men\">  </i>Logout</a></li>\n");
      out.write("            </ul>\n");
      out.write("        </div>\n");
      out.write("        <div id=\"header-wrapper\">\n");
      out.write("            <div id=\"header\" class=\"container\">\n");
      out.write("                <div id=\"logo\">\n");
      out.write("                </div>\n");
      out.write("                <div id=\"menu\">\n");
      out.write("                    <ul>\n");
      out.write("                        <li class=\"current_page_item\"><a href=\"indexLOGIN.jsp\">Home</a></li>\n");
      out.write("                        <li><a href=\"Event_Log.jsp\">Event Log</a></li>\n");
      out.write("                        <li><a href=\"Schedule.jsp\">Schedule</a></li> \n");
      out.write("                        <li><a href=\"File_Sharing.jsp\">File Sharing</a></li>\n");
      out.write("                    </ul>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <div id=\"banner\">\n");
      out.write("                <div class=\"content\"><img src=\"../images/img02.jpg\" width=\"1000\" height=\"400\" alt=\"\" /></div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <div id=\"page\">\n");
      out.write("            <div class=\"clearfix\"> </div>\n");
      out.write("            <!---->\n");
      out.write("        </div>\n");
      out.write("        <div class=\"clearfix\"> </div>\n");
      out.write("            \n");
      out.write("    </div>\n");
      out.write("    <!---->\n");
      out.write("    <div id=\"page\">\n");
      out.write("        <div class=\"clearfix\"> </div>\n");
      out.write("        <h2 class=\"title\">Selamat Datang di Laboratorium Pemrograman Sistem Informasi</h2>\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <div class=\" login-left\">\n");
      out.write("                <div class=\"back\">\n");
      out.write("                    <div class=\"login-left\">\n");
      out.write("                        <div class=\"entry\">\n");
      out.write("                            <p>Sampai dengan tahun 2014, Jurusan Sistem Informasi ITS Surabaya memiliki fasilitas 4 laboratorium dan salah satunya yaitu Laboratorium Pemrograman Sistem Informasi atau biasa disebut dengan LPSI. LPSI merupakan laboratorium yang digunakan sebagai sarana penunjang untuk melakukan aktivitas pembelajaran dalam kuliah di Jurusan Sistem Informasi ITS Surabaya</p>\n");
      out.write("                            <p>Selain digunakan sebagai sarana penunjang pembelajaran mahasiswa, lab ini juga sering digunakan untuk mengerjakan beberapa tugas di luar praktikum yang telah terjadwal.<br />\n");
      out.write("                            </p>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("    <!---->\n");
      out.write("    <div id=\"page\">\n");
      out.write("        <div class=\"clearfix\"> </div>\n");
      out.write("        <h2 class=\"title\">Contact</h2>\n");
      out.write("        <div class=\"entry\">\n");
      out.write("            <p><img src=\"../images/telepon.jpg\" width=\"25\" height=\"25\"> 081-232-1311-32</img>\n");
      out.write("            </p>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("                </div>\n");
      out.write("                <div style=\"clear: both;\">&nbsp;</div>\n");
      out.write("                    \n");
      out.write("            </div>\n");
      out.write("                \n");
      out.write("        </div>\n");
      out.write("            \n");
      out.write("    </div>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
