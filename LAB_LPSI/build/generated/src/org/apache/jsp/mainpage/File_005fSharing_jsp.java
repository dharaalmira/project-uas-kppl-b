package org.apache.jsp.mainpage;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class File_005fSharing_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n");
      out.write("<head>\r\n");
      out.write("<meta name=\"keywords\" content=\"\" />\r\n");
      out.write("<meta name=\"description\" content=\"\" />\r\n");
      out.write("<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("        <title>Laboratorium Pemograman Sistem Informasi</title>\r\n");
      out.write("<link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,300,600\" rel=\"stylesheet\" type=\"text/css\" />\r\n");
      out.write("<link href='http://fonts.googleapis.com/css?family=Abel|Satisfy' rel='stylesheet' type='text/css' />\r\n");
      out.write("<link href=\"style.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\r\n");
      out.write("</head>\r\n");
      out.write("    \r\n");
      out.write("    <body>\r\n");
      out.write("<div id=\"wrapper\">\r\n");
      out.write("    <h1>Jurusan Sistem Informasi</h1>\r\n");
      out.write("       <p> Institut Teknologi Sepuluh Nopember </p>\r\n");
      out.write("       <div class=\"header-info\">\r\n");
      out.write("            <ul class=\"support-right\">\r\n");
      out.write("                <li><a href=\"index.jsp\"> Welcome ");
      out.print(session.getAttribute("username"));
      out.write(" <i class=\"glyphicon glyphicon-user\" class=\"men\">  </i>Logout</a></li>\r\n");
      out.write("            </ul>\r\n");
      out.write("       </div>\r\n");
      out.write("\t<div id=\"header-wrapper\">\r\n");
      out.write("\t\t<div id=\"header\" class=\"container\">\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t\t\t<div id=\"menu\">\r\n");
      out.write("\t\t\t\t<ul>\r\n");
      out.write("\t\t\t\t\t<li ><a href=\"indexLOGIN.jsp\">Home</a></li>\r\n");
      out.write("\t\t\t\t\t<li ><a href=\"Event_Log.jsp\">Event Log</a></li>\r\n");
      out.write("\t\t\t\t\t<li ><a href=\"Schedule.jsp\">Schedules</a></li> \r\n");
      out.write("\t\t\t\t\t<li class=\"current_page_item\"><a href=\"File_Sharing.jsp\">File Sharing</a></li>\r\n");
      out.write("                                        <li><a href=\"user.jsp\">User</a></li>\r\n");
      out.write("\t\t\t\t</ul>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("            <div id=\"banner\">\r\n");
      out.write("            <div class=\"content\"><img src=\"../images/Sharing_File.jpg\" width=\"1000\" height=\"400\" alt=\"\" /></div>\r\n");
      out.write("            </div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t<div id=\"page\">\r\n");
      out.write("            <div class=\"clearfix\"> </div>\r\n");
      out.write("\t\t\t\t\t<!---->\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\t\t\t<div class=\"clearfix\"> </div>\r\n");
      out.write("\t\t</div>\t\r\n");
      out.write(" \t\t\t\t<div class=\"clearfix\"> </div>\t\r\n");
      out.write("                        <!----> \r\n");
      out.write("                        <div id=\"page\">\r\n");
      out.write("                            <div class=\"clearfix\"> </div>\r\n");
      out.write("                            <h2 class=\"title\">FILE SHARING</h2>\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <!---->\r\n");
      out.write("                        <div class=\"container\">\r\n");
      out.write("                            <div class=\" login-left\">\r\n");
      out.write("                                <h3>Ketentuan Penggunaan File Sharing:</h3>\r\n");
      out.write("                               <TMPL_VAR site_name> Perjanjian Layanan ini menggambarkan syarat dan ketentuan yang kami tawarkan kepada Anda. Dengan menggunakan layanan kami, Pengguna setuju untuk terikat oleh syarat dan ketentuan berikut: <TMPL_VAR site_name>\r\n");
      out.write("                                        <ol> \r\n");
      out.write("                                            <li>Kami berhak untuk menonaktifkan menghubungkan langsung pada account pengguna yang menggunakan bandwidth yang berlebihan atau sebaliknya menyalahgunakan sistem.</li>\r\n");
      out.write("                                            <li>Pornografi, gambar seksual dan jenis gambar atau video ofensif dilarang. Materi berhak cipta juga dilarang. Kami berhak untuk memutuskan konten yang tidak sesuai dan dapat menghapus gambar atau video setiap saat tanpa pemberitahuan Pengguna.</li>\r\n");
      out.write("                                            <li>Pengguna harus setuju untuk mematuhi semua undang-undang yang berlaku,termasuk hak cipta dan merek dagang hukum. Gambar, video dan file yang melanggar hak copy atau merek dagang tidak diperbolehkan. Jika seseorang memiliki klaim pelanggaran terhadap Anda, Anda akan diminta untuk menghapus file yang dilindungi hak cipta sampai masalah teratasi. Jika ada sengketa antara peserta di situs ini,kami tidak berkewajiban untuk terlibat.</li>\r\n");
      out.write("                                        </ol>\r\n");
      out.write("<!---->\r\n");
      out.write("<Script type=\"text/javascript\">\r\n");
      out.write("var ext_allowed='<TMPL_VAR ext_allowed>';\r\n");
      out.write("var ext_not_allowed='<TMPL_VAR ext_not_allowed>';\r\n");
      out.write("var max_upload_files='<TMPL_VAR max_upload_files>';\r\n");
      out.write("var max_upload_size='<TMPL_VAR max_upload_size>';\r\n");
      out.write("var descr_mode='<TMPL_VAR enable_file_descr>';\r\n");
      out.write("</Script>\r\n");
      out.write("\r\n");
      out.write("<h3>Form Upload</h3>\r\n");
      out.write("<p><b> Up to 1 file max </b></p>\r\n");
      out.write("<form enctype=\"multipart/form-data\" action=\"uploadfile.jsp\" method=\"post\" onsubmit=\"return verify()\">       \r\n");
      out.write("    <table border='1'>\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td>\r\n");
      out.write("                <br/>\r\n");
      out.write("                File yang bisa di upload berupa file MS-word, MS-Excel, txt dan PDF files. max size = 15mb\r\n");
      out.write("                <br/>\r\n");
      out.write("                <br/>\r\n");
      out.write("            </td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td>\r\n");
      out.write("                <input type=\"file\" name=\"filename\" id=\"filename\" accept=\".txt, application/pdf, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword\"  />                                                            \r\n");
      out.write("            </td>\r\n");
      out.write("        </tr>\r\n");
      out.write("    </table>\r\n");
      out.write("    <p></p>\r\n");
      out.write("    <a>Saya telah membaca dan setuju dengan ketentuan diatas</a>\r\n");
      out.write("    <input type=\"checkbox\" name=\"tos\" value=\"1\" id=\"tos\" checked onClick=\"if(this.checked){$('submit_btn').disabled=false}else{$('submit_btn').disabled=true};this.blur();\">\r\n");
      out.write("        <p><input type=\"submit\" value=\"Save File\"></p>\r\n");
      out.write("</form>\r\n");
      out.write("<br/>\r\n");
      out.write("<br/>\r\n");
      out.write("<h3>Download file</h3>\r\n");
      out.write("<a href=\"downloadfile.jsp\"> Klik disini untuk melihat dan download file</a>\r\n");
      out.write("<div class=\"clearfix\"> </div>\r\n");
      out.write("\r\n");
      out.write("</script>\r\n");
      out.write("<a href=\"#to-top\" id=\"toTop\" style=\"display: block;\"> <span id=\"toTopHover\" style=\"opacity: 1;\"> </span></a>\r\n");
      out.write("<!----> \r\n");
      out.write("<div style=\"clear: both;\">&nbsp;</div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
