package org.apache.jsp.mainpage;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.Connection;

public final class Schedule_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n");
      out.write("<head>\r\n");
      out.write("<meta name=\"keywords\" content=\"\" />\r\n");
      out.write("<meta name=\"description\" content=\"\" />\r\n");
      out.write("<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("        <title>Laboratorium Pemograman Sistem Informasi</title>\r\n");
      out.write("<link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,300,600\" rel=\"stylesheet\" type=\"text/css\" />\r\n");
      out.write("<link href='http://fonts.googleapis.com/css?family=Abel|Satisfy' rel='stylesheet' type='text/css' />\r\n");
      out.write("<link href=\"style.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\r\n");
      out.write("</head>\r\n");
      out.write("    \r\n");
      out.write("    <body>\r\n");
      out.write("<div id=\"wrapper\">\r\n");
      out.write("    <h1>Jurusan Sistem Informasi</h1>\r\n");
      out.write("       <p> Institut Teknologi Sepuluh Nopember </p>\r\n");
      out.write("       <div class=\"header-info\">\r\n");
      out.write("            <ul class=\"support-right\">\r\n");
      out.write("                <li><a href=\"index.jsp\"> Welcome ");
      out.print(session.getAttribute("username"));
      out.write(" <i class=\"glyphicon glyphicon-user\" class=\"men\">  </i>Logout</a></li>\r\n");
      out.write("            </ul>\r\n");
      out.write("       </div>\r\n");
      out.write("\t<div id=\"header-wrapper\">\r\n");
      out.write("\t\t<div id=\"header\" class=\"container\">\r\n");
      out.write("\t\t\t<div id=\"logo\">\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t\t<div id=\"menu\">\r\n");
      out.write("\t\t\t\t<ul>\r\n");
      out.write("\t\t\t\t\t<li ><a href=\"indexLOGIN.jsp\">Home</a></li>\r\n");
      out.write("\t\t\t\t\t<li ><a href=\"Event_Log.jsp\">Event Log</a></li>\r\n");
      out.write("\t\t\t\t\t<li class=\"current_page_item\"><a href=\"Schedule.jsp\">Schedules</a></li> \r\n");
      out.write("\t\t\t\t\t<li><a href=\"File_Sharing.jsp\">File Sharing</a></li>\r\n");
      out.write("\t\t\t\t</ul>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\r\n");
      out.write("               \r\n");
      out.write("\t\t\r\n");
      out.write("            \r\n");
      out.write("\t\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t<div style=\"clear: both;\">&nbsp;</div>\r\n");
      out.write("\t\r\n");
      out.write("    <style type=\"text/css\">\r\n");
      out.write("    body\r\n");
      out.write("    {\r\n");
      out.write("        font-family: arial;\r\n");
      out.write("    }\r\n");
      out.write("\r\n");
      out.write("    th,td\r\n");
      out.write("    {\r\n");
      out.write("        margin: 0;\r\n");
      out.write("        text-align: center;\r\n");
      out.write("        border-collapse: collapse;\r\n");
      out.write("        outline: 1px solid #e3e3e3;\r\n");
      out.write("    }\r\n");
      out.write("\r\n");
      out.write("    td\r\n");
      out.write("    {\r\n");
      out.write("        padding: 5px 10px;\r\n");
      out.write("        \r\n");
      out.write("    }\r\n");
      out.write("\r\n");
      out.write("    th\r\n");
      out.write("    {\r\n");
      out.write("        background: #666;\r\n");
      out.write("        color: white;\r\n");
      out.write("        padding: 5px 10px;\r\n");
      out.write("    }\r\n");
      out.write("\r\n");
      out.write("    td:hover\r\n");
      out.write("    {\r\n");
      out.write("        cursor: pointer;\r\n");
      out.write("        background: #666;\r\n");
      out.write("        color: white;\r\n");
      out.write("    }\r\n");
      out.write("    </style>\r\n");
      out.write("    <body>\r\n");
      out.write("        <center>\r\n");
      out.write("                    \r\n");
      out.write("                        <thead>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <th colspan=\"2\"><font color =white face=\"Geneva, Arial\" size=\"4\">\r\n");
      out.write("                                        Schedule LPSI</font></th>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                        </thead>\r\n");
      out.write("    <table width=\"80%\" align=\"center\" >\r\n");
      out.write("    <div id=\"head_nav\">\r\n");
      out.write("    <tr>\r\n");
      out.write("        <th>Time</th>\r\n");
      out.write("        <th> 08am - 11am</th>\r\n");
      out.write("        <th> 11pm - 13pm</th>\r\n");
      out.write("        <th> 13pm - 15pm</th>\r\n");
      out.write("        <th> 15pm - 17pm</th>\r\n");
      out.write("        <th> 17pm - 19pm</th>\r\n");
      out.write("        <th> 19pm - 21pm</th>\r\n");
      out.write("        \r\n");
      out.write("    </tr>\r\n");
      out.write("    </div>  \r\n");
      out.write("\r\n");
      out.write("    <tr>\r\n");
      out.write("        <th>Monday</th>\r\n");
      out.write("        \r\n");
      out.write("            <td>Alpro</td>\r\n");
      out.write("            <td>PSDP</td>\r\n");
      out.write("            <td title=\"No Class\" class=\"Holiday\"></td>\r\n");
      out.write("            <td>PBO</td>\r\n");
      out.write("            <td>KPPL</td>\r\n");
      out.write("            <td title=\"No Class\" class=\"Holiday\"></td>     \r\n");
      out.write("        \r\n");
      out.write("    </tr>\r\n");
      out.write("\r\n");
      out.write("    <tr>\r\n");
      out.write("        <th>Tuesday</th>\r\n");
      out.write("       \r\n");
      out.write("            <td>Alpro</td>\r\n");
      out.write("            <td>PSDP</td>\r\n");
      out.write("            <td title=\"No Class\" class=\"Holiday\"></td>\r\n");
      out.write("            <td>PBO</td>\r\n");
      out.write("            <td>KPPL</td>\r\n");
      out.write("            <td title=\"No Class\" class=\"Holiday\"></td>\r\n");
      out.write("        \r\n");
      out.write("    </tr>\r\n");
      out.write("\r\n");
      out.write("    <tr>\r\n");
      out.write("        <th>Wednesday</th>\r\n");
      out.write("        \r\n");
      out.write("           <td>Alpro</td>\r\n");
      out.write("            <td>PSDP</td>\r\n");
      out.write("            <td title=\"No Class\" class=\"Holiday\"></td>\r\n");
      out.write("            <td>PBO</td>\r\n");
      out.write("            <td>KPPL</td>\r\n");
      out.write("            <td title=\"No Class\" class=\"Holiday\"></td>\r\n");
      out.write("\r\n");
      out.write("       \r\n");
      out.write("    </tr>\r\n");
      out.write("\r\n");
      out.write("    <tr>\r\n");
      out.write("        <th>Thursday</th>\r\n");
      out.write("        \r\n");
      out.write("            <td>Alpro</td>\r\n");
      out.write("            <td>PSDP</td>\r\n");
      out.write("            <td title=\"No Class\" class=\"Holiday\"></td>\r\n");
      out.write("            <td>PBO</td>\r\n");
      out.write("            <td>KPPL</td>\r\n");
      out.write("            <td title=\"No Class\" class=\"Holiday\"></td>\r\n");
      out.write("\r\n");
      out.write("        \r\n");
      out.write("    </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        \r\n");
      out.write("\r\n");
      out.write("            <div>\r\n");
      out.write("                <p><input type=\"button\" name=\"edit\" value=\"Edit\" onClick=\"javascript:window.location='editSchedule.jsp';\" style=\"background-color:#49743D;font-weight:bold;color:blue;\">\r\n");
      out.write("              <!--  <p><input type=\"submit\" value=\"Edit Schedule\" ><a href=\"editSchedule.jsp\"></a> </p>    -->           \r\n");
      out.write("            </div> \r\n");
      out.write("              \r\n");
      out.write("            \r\n");
      out.write("<div style=\"clear: both;\">&nbsp;</div>\r\n");
      out.write("            </body>\r\n");
      out.write("        </div>\r\n");
      out.write("        \r\n");
      out.write("            <div id=\"page\">\r\n");
      out.write("            <div style=\"clear: both;\">&nbsp;</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("           \r\n");
      out.write("</body>\r\n");
      out.write("      \r\n");
      out.write("</html>\r\n");
      out.write("\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
