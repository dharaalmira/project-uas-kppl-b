-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 01, 2016 at 04:38 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `members`
--

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `NRP` varchar(15) NOT NULL,
  `Nama` varchar(30) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`NRP`, `Nama`, `Email`, `Username`, `Password`) VALUES
('2147483647', 'Almira', 'almiradhara@gmail.com', 'dharaalmira', 'roeaeoropew'),
('5213100076', 'Ashma Hanifah', 'ashmahanifah@gmail.com', 'ashmahanifah', 'lalala'),
('5213100184', 'Almira', 'almiradhara@gmail.com', 'dharaalmira', 'tae'),
('5213100187', 'Izzatun Nafsi', 'izzalkarimah@gmail.com', 'izza', 'hai'),
('5213100189', 'asrar', 'asrar@gmail.com', 'asrar', 'tae'),
('5213100198', 'dhara', 'almiradhara@yahoo.com', 'dharaalmira', 'taem'),
('5213100199', 'al', 'dharaalmira@hotmail.com', 'dharaaa', 'tae');

-- --------------------------------------------------------

--
-- Table structure for table `scheduler`
--

CREATE TABLE IF NOT EXISTS `scheduler` (
  `Tanggal Booking` varchar(20) NOT NULL,
  `book1` varchar(20) NOT NULL,
  `book2` varchar(20) NOT NULL,
  `book3` varchar(20) NOT NULL,
  `book4` varchar(20) NOT NULL,
  `book5` varchar(20) NOT NULL,
  `book6` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheduler`
--

INSERT INTO `scheduler` (`Tanggal Booking`, `book1`, `book2`, `book3`, `book4`, `book5`, `book6`) VALUES
('null', 'Alpro', 'PSDP', ' ', 'DMJK ', ' ', ' '),
('null', 'Alpro', 'PSDP', ' ', 'DMJK ', ' ', ' '),
('null', 'Alpro', 'PSDP', ' ', 'DMJK ', ' ', ' '),
('null', 'Alpro', 'PSDP', 'KAI', 'DMJK ', ' ', ' '),
('null', 'Alpro', 'PSDP', ' KAI', 'DMJK ', ' ', ' '),
('null', 'Alpro', 'PSDP', ' KAI', 'DMJK ', ' ', ' '),
('null', 'Alpro', 'PSDP', 'KAI', 'DMJK ', ' ', ' '),
('null', 'Alpro', 'PSDP', ' ', 'DMJK ', ' ', ' ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`NRP`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
